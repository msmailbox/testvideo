package com.avantaj.phonegap;

import org.apache.cordova.DroidGap;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.DownloadListener;

public class HelloCordovaActivity extends DroidGap {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.loadUrl("file:///android_asset/www/index2.html");

		appView.setDownloadListener(new DownloadListener()

		{

			public void onDownloadStart(String url, String userAgent,
					String contentDisposition, String mimeType, long size)

			{

				Intent viewIntent = new Intent(Intent.ACTION_VIEW);
				viewIntent.setDataAndType(Uri.parse(url), mimeType);
				try
				{
					startActivity(viewIntent);
				}
				catch (ActivityNotFoundException ex)
				{
					Log.w("YourLogTag",
							"Couldn't find activity to view mimetype: "
									+ mimeType);
				}
			}
		});
	}

}